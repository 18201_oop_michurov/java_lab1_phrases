# Phrase Counter

Count phrases in a text and print those which were frequent enough.

## Usage

```bash
$ java -jar phrase-counter[-stream-api].jar [INPUT_FILE] [KEYS]
```

#### Keys

    -n, --n {PHRASE_LENGTH}       (optional) phrase length in words (2 if not specified)

    -m, --m {MIN_OCCURRENCES}     (optional) minimal number of occurrences for a phrase to be present in output (2 if not specified)

    -h, --help                    display help
    
## BUILD & EXECUTION

#### Dependencies

* [Gradle](https://gradle.org/)
* [JUnit5](https://junit.org/junit5/)

#### Build jars

```bash
$ ./gradlew jar
```

Output dir: build/libs

#### Run tests

```bash
$ ./gradlew check
```
