package ru.nsu.g.mmichurov.phrasecounter.util.phrases;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class Phrases {

    public static void processAsStream(
            InputStream inputStream, PrintStream outputStream,
            int phraseLength, int lowerBound) {
        var it = new Phrases.PhraseIterator(inputStream, phraseLength);

        StreamSupport.stream(((Iterable<String>) () -> it).spliterator(), false)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .filter(x -> x.getValue() >= lowerBound)
                .sorted((e1, e2) -> (int) (e1.getValue().equals(e2.getValue())
                        ? e1.getKey().compareTo(e2.getKey())
                        : e2.getValue() - e1.getValue()))
                .forEach(x -> outputStream.printf("%s (%d)\n", x.getKey(), x.getValue()));
    }

    public static void processAsUsual(
            InputStream inputStream, PrintStream outputStream,
            int phraseLength, int lowerBound) {
        var counts = Phrases.countPhrases(inputStream, phraseLength).entrySet();
        counts.removeIf(x -> x.getValue() < lowerBound);

        var list = new ArrayList<>(counts);
        list.sort((e1, e2) -> (e1.getValue().equals(e2.getValue())
                ? e1.getKey().compareTo(e2.getKey())
                : e2.getValue() - e1.getValue()));

        for (var item : list) {
            outputStream.printf("%s (%d)\n", item.getKey(), item.getValue());
        }
    }

    static HashMap<String, Integer> countPhrases(InputStream inputStream, int phraseLength)
            throws IllegalArgumentException {
        PhraseIterator it = new PhraseIterator(inputStream, phraseLength);
        HashMap<String, Integer> occurrences = new HashMap<>();

        for (String phrase : (Iterable<String>) () -> it) {
            int currentCount = 0;

            if (occurrences.containsKey(phrase)) {
                currentCount = occurrences.get(phrase);
            }

            occurrences.put(phrase, currentCount + 1);
        }

        return occurrences;
    }

    static class PhraseIterator implements Iterator<String> {

        private Scanner scanner;
        private StringBuffer buffer;

        public PhraseIterator(InputStream source, int phraseLength) {
            if (phraseLength <= 0) {
                throw new IllegalArgumentException("Phrase Length must be a positive number");
            }

            this.scanner = new Scanner(source);
            this.buffer = new StringBuffer();

            for (int i = 0; i < phraseLength - 1 && this.scanner.hasNext(); ++i) {
                this.buffer.append(' ');
                this.buffer.append(this.scanner.next());
            }
        }

        @Override
        public boolean hasNext() {
            return this.scanner.hasNext();
        }

        @Override
        public String next() {
            this.constructNextPhrase();
            return this.buffer.toString();
        }

        private void constructNextPhrase() {
            this.buffer.append(' ');
            this.buffer.append(this.scanner.next());
            this.buffer.delete(0, this.buffer.indexOf(" ") + 1);
        }
    }

}
