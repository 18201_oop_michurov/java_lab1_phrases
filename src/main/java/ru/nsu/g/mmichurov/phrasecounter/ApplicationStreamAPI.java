package ru.nsu.g.mmichurov.phrasecounter;

import ru.nsu.g.mmichurov.phrasecounter.util.ArgumentParser;
import ru.nsu.g.mmichurov.phrasecounter.util.ArgumentParser.*;
import ru.nsu.g.mmichurov.phrasecounter.util.phrases.Phrases;

import java.io.*;

public class ApplicationStreamAPI {

    public static void main(String[] args) {
        IntegerArgument n = new IntegerArgument('n', "n", 2);
        IntegerArgument m = new IntegerArgument('m', "m", 2);
        Flag help = new Flag('h', "help");

        ArgumentParser parser = new ArgumentParser(m, n, help);

        parser.parse(args);

        if (parser.getOption('h').isFound()) {
            printHelp();
        }

        var inputFileName = parser.getFreeArguments().isEmpty()
                || parser.getFreeArguments().get(0).equals("-")
                ? null
                : parser.getFreeArguments().get(0);

        try (var inputStream = inputFileName == null
                ? System.in
                : new FileInputStream(new File(inputFileName))) {
            Phrases.processAsStream(inputStream, System.out, n.getArgument(), m.getArgument());
        } catch (FileNotFoundException e) {
            System.out.printf("File \"%s\" not found", inputFileName);
            System.exit(-1);
        } catch (IOException e) {
            System.out.println("An error occurred when reading form source");
            System.exit(-1);
        }
    }

    static void printHelp() {
        System.out.print(
                "Count phrases in a text and print those which were frequent enough.\n\n" +
                        "USAGE:\n    <executable> [INPUT_FILE] [KEYS]\n\nKEYS:" +
                        "\n\n    -n, --n {PHRASE_LENGTH}       (optional) phrase length in words " +
                        "(2 if not specified)" +
                        "\n\n    -m, --m {MIN_OCCURRENCES}     (optional) minimal number of occurrences " +
                        "for a phrase to be present in output (2 if not specified)" +
                        "\n\n    -h, --help                    display help\n\n"
        );
    }
}
