package ru.nsu.g.mmichurov.phrasecounter.util.phrases;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;


class PhrasesTest {
    static final String input1 =
            "we all live in a yellow submarine\n" +
                    "yellow submarine yellow submarine\n" +
                    "we all live in a yellow submarine\n" +
                    "yellow submarine yellow submarine";

    static final String input2 =
            "let it be, let it be, let it be, let it be\n" +
                    "Yeah, there will be an answer, let it be\n" +
                    "let it be, let it be, let it be, let it be\n" +
                    "Whisper words of wisdom, let it be\n";

    @Test
    void testCountPhrases() {
        Map<String, Integer> occurrences =
                Phrases.countPhrases(new ByteArrayInputStream(input1.getBytes()), 3);

        assertEquals(new HashMap<String, Integer>() {{
            put("yellow submarine yellow", 4);
            put("submarine yellow submarine", 4);
            put("we all live", 2);
            put("all live in", 2);
            put("live in a", 2);
            put("in a yellow", 2);
            put("a yellow submarine", 2);
            put("yellow submarine we", 1);
            put("submarine we all", 1);
        }}.entrySet(), occurrences.entrySet());

        String emptyInput = "";

        occurrences = Phrases.countPhrases(new ByteArrayInputStream(emptyInput.getBytes()), 1);

        assertTrue(occurrences.isEmpty());
    }

    @Test
    void testProcess() {
        var inputStream = new ByteArrayInputStream(input1.getBytes());
        var outputStream = new ByteArrayOutputStream();
        var printStream = new PrintStream(outputStream);

        Phrases.processAsUsual(inputStream, printStream, 3, 4);
        assertEquals("submarine yellow submarine (4)\nyellow submarine yellow (4)\n", outputStream.toString());

        inputStream.reset();
        outputStream.reset();

        Phrases.processAsStream(inputStream, printStream, 3, 4);
        assertEquals("submarine yellow submarine (4)\nyellow submarine yellow (4)\n", outputStream.toString());

        inputStream = new ByteArrayInputStream(input2.getBytes());
        outputStream.reset();

        Phrases.processAsUsual(inputStream, printStream, 3, 4);
        assertEquals("be, let it (6)\nit be, let (6)\nlet it be, (6)\nlet it be (4)\n", outputStream.toString());

        inputStream.reset();
        outputStream.reset();

        Phrases.processAsStream(inputStream, printStream, 3, 4);
        assertEquals("be, let it (6)\nit be, let (6)\nlet it be, (6)\nlet it be (4)\n", outputStream.toString());

        inputStream = new ByteArrayInputStream("".getBytes());
        outputStream.reset();

        Phrases.processAsUsual(inputStream, printStream, 3, 4);
        assertTrue(outputStream.toString().isEmpty());

        inputStream.reset();
        outputStream.reset();

        Phrases.processAsStream(inputStream, printStream, 3, 4);
        assertTrue(outputStream.toString().isEmpty());
    }

    static class PhraseIteratorTest {

        @Test
        void testEmptyInput() {
            String empty = "";

            Phrases.PhraseIterator it = new Phrases.PhraseIterator(new ByteArrayInputStream(empty.getBytes()), 2);

            assertFalse(it.hasNext());
        }

        @Test
        void testInvalidParameters() {
            assertThrows(IllegalArgumentException.class, () -> new Phrases.PhraseIterator(System.in, 0));
        }

        @Test
        void testPhraseConstruction() {
            String input = "w1";
            Phrases.PhraseIterator it = new Phrases.PhraseIterator(new ByteArrayInputStream(input.getBytes()), 1);

            assertTrue(it.hasNext());
            assertEquals(it.next(), "w1");
            assertFalse(it.hasNext());

            input = "w1 w2";
            it = new Phrases.PhraseIterator(new ByteArrayInputStream(input.getBytes()), 2);

            assertTrue(it.hasNext());
            assertEquals(it.next(), "w1 w2");
            assertFalse(it.hasNext());

            input = "Is this the price I'm paying for my past mistakes?";
            it = new Phrases.PhraseIterator(new ByteArrayInputStream(input.getBytes()), 7);

            StringBuilder os = new StringBuilder();

            while (it.hasNext()) {
                os.append(it.next()).append('\n');
            }

            assertEquals(
                    "Is this the price I'm paying for\n" +
                            "this the price I'm paying for my\n" +
                            "the price I'm paying for my past\n" +
                            "price I'm paying for my past mistakes?\n",
                    os.toString()
            );
        }
    }
}
